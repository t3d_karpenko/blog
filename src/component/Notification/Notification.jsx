import './style.scss';

const Notification = ({ title, message, color }) => {
  return (
    <div
      className="iteamNotification"
      style={{
        boxShadow: `4px 8px 0px ${color}`,
        border: `1px solid ${color}`,
      }}
    >
      <div className="titleNatification">{title}</div>
      <div className="messageNatification">{message}</div>
    </div>
  );
};

export default Notification;
