import { url } from '../../src/config';
import './style.scss';

const Avatar = ({ user }) => {
  return (
    <>
      {user?.avatar ? (
        <img className="avatarUser" src={url + '/' + user.avatar} alt="" />
      ) : (
        <div className="avatarUserLetter">{user?.email?.[0]}</div>
      )}
    </>
  );
};

export default Avatar;
