import { ReactComponent as Back } from '../../../assets/undo.svg';
import { ReactComponent as SearchImg } from '../../../assets/glass-search.svg';
import useAuth from '../../../hooks/useAuth';
import { Link } from 'react-router-dom';
import './style.scss';
import AdminTable from './AdminTable';
import { useState } from 'react';

const PageAdmin = () => {
  const { users } = useAuth();

  const [search, setSearch] = useState('')

  return (
    <div className="wrapperPageAdmin">
      <div className="containerSearchPageAdmin">
        <div className="headerPageAdmin">
          <Link className="containerBack" to="/">
            <Back className="backPageAdmin" />
          </Link>

          <div className="searchPageAdmin">
            <SearchImg className="searchImg" />
            <input placeholder="Search.." type="search"  value={search} onChange={e => setSearch(e.target.value)}/>
          </div>
          <div className="addUserButton">+User</div>
        </div>
      </div>

      <div className="containerAdminTable">
        <div className="titleAdminTable">
          <div>Avatar</div>
          <div>Name</div>
          <div>Email</div>
          <div>Phone</div>
          <div>Role</div>
          <div>Action</div>
        </div>
        <div className="listAdminTable">
          {users?.filter(item =>  item.email.includes(search) ||  item.name.includes(search)).map(item => (
            <AdminTable item={item} key={item._id} search={search} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default PageAdmin;
