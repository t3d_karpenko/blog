import { ReactComponent as EditImg } from '../../../../assets/edit.svg';
import { ReactComponent as Delete } from '../../../../assets/delete.svg';
import './style.scss';
import Avatar from '../../../../component/avatar';

const AdminTable = ({ item, search}) => {
  return (
    <div className="wrapperAdminTable">
      <div className="userPhotoAdminTable">
        <Avatar user={item} />
      </div>
      <div className="">{item?.name}</div>
      <div className="">{item?.email}</div>
      <div className="">{item?.phone}</div>
      <div className="">{item?.role}</div>
      <div className="actionAdminTable">
        <EditImg className="actionEdit" />
        <Delete className="actionDelete" />
      </div>
    </div>
  );
};
export default AdminTable;
