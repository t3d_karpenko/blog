import { Link } from 'react-router-dom';
import { useState } from 'react';
import './style.scss';

const NavMenu = () => {
  const [menuActive, setMenuActive] = useState(false);

  return (
    <div className="navMenuWrapper">
      <div
        className={
          menuActive ? 'burgerNavMenuWrapper  active' : 'burgerNavMenuWrapper'
        }
      >
        <div className="burgerNavMenuItem">
          <Link
            className="MenuItem"
            to="/profile"
            onClick={() => setMenuActive(prev => !prev)}
          >
            Profile
          </Link>

          <Link
            className="MenuItem"
            to="/"
            onClick={() => setMenuActive(prev => !prev)}
          >
            Blog
          </Link>

          <Link
            className="MenuItem"
            to="/create_post"
            onClick={() => setMenuActive(prev => !prev)}
          >
            New Post
          </Link>
        </div>
      </div>

      <div
        className={menuActive ? 'overlay  active' : 'overlay'}
        onClick={() => setMenuActive(prev => !prev)}
      />

      <div className="navMenuButton" onClick={() => setMenuActive(!menuActive)}>
        <button className="itemNavMenuButton">
          <span className="itemNavMenu">Menu</span>
        </button>
      </div>
    </div>
  );
};

export default NavMenu;
