import './style.scss';
import useAuth from '../../../../hooks/useAuth';

const LogOut = () => {
  const { logout } = useAuth();

  return (
    <div>
      <div
        className="logOutButton"
        onClick={() => {
          logout();
        }}
      >
        Exit
      </div>
    </div>
  );
};

export default LogOut;
