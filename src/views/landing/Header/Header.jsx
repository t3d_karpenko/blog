import LogOut from './LogOut';
import Avatar from '../../../component/avatar';
import { Link } from 'react-router-dom';
import { ReactComponent as LogoImg } from '../../../assets/photos.svg';
import { useState } from 'react';
import useAuth from '../../../hooks/useAuth';
import './style.scss';

import NavMenu from './NavMenu';

const Header = () => {
  const { user } = useAuth();

  const [menuUserActive, setMenuUserActive] = useState(false);

  return (
    <div className="wrapperHeader">
      <div className="iteamNav">
        <div className="headerLogoImg">
          <LogoImg />
        </div>

        <div className="profileNavButton">
          <Link to="/profile"> Profile</Link>
        </div>
        <div className="blogNavButton">
          <Link to="/"> Blog</Link>
        </div>
        <div className="newPostNavButton">
          <Link to="/create_post">New Post</Link>
        </div>

        <NavMenu />
      </div>

      <div
        className={
          menuUserActive ? 'overlayMenuUser  active' : 'overlayMenuUser'
        }
        onClick={() => setMenuUserActive(prev => !prev)}
      />
      <div
        className={
          menuUserActive ? 'headerUserPhoto active' : 'headerUserPhoto'
        }
        onClick={() => setMenuUserActive(prev => !prev)}
      >
        <Avatar user={user} />
      </div>
      <div className={menuUserActive ? 'rhombus active' : 'rhombus '}></div>
      <div
        className={menuUserActive ? 'headerUserMenu active' : 'headerUserMenu '}
      >
        <div
          className="profileItemUserMenu"
          onClick={() => setMenuUserActive(prev => !prev)}
        >
          <Link to="/profile">Profile</Link>
          <Link to="/admin">Admin Menu</Link>
        </div>
        <LogOut />
      </div>
    </div>
  );
};

export default Header;
