import { Link } from 'react-router-dom';
import './style.scss';
import { url } from '../../../../config';

const Post = ({ iteam }) => {
  return (
    <Link
      to={iteam._id ? `/post/${iteam._id}` : '/create_post'}
      className="wrapperPost"
    >
      <img
        className="imgPost"
        src={
          iteam.cover.slice(0, 4) === 'http' ||
          iteam.cover.slice(0, 4) === 'blob'
            ? iteam.cover
            : url + '/' + iteam.cover
        }
        alt="postImg"
      />
      <div className="iteamPost">
        <div className="titlePost">{iteam.title}</div>
        <div className="bodyPost">{iteam.description}</div>
      </div>
    </Link>
  );
};

export default Post;
