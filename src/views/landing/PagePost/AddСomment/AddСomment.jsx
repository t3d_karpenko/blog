import axios from 'axios';
import { useState } from 'react';
import useAuth from '../../../../hooks/useAuth';
import { ReactComponent as SendComment } from '../../../../assets/send.svg';
import Avatar from '../../../../component/avatar';

import './style.scss';

const AddСomment = ({ id, setItemComment }) => {
  const { user } = useAuth();

  const [textComment, setTextComment] = useState('');

  const submitСomment = e => {
    e.preventDefault();
    if (!textComment) return;
    axios
      .post(
        'https://22k.space/api/comment/' + id,
        { body: textComment },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('tokenToken')}`,
          },
        },
      )
      .then(res => {
        console.log(res);
        setItemComment(prev => [res.data, ...prev]);
        setTextComment('');
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <div className="addCommentWrapper">
      <div className="authorPostPhoto">
        <Avatar user={user} />
      </div>

      <div className="blockAddComment">
        <form onSubmit={submitСomment}>
          <input
            type="text"
            maxLength="140"
            placeholder="write comment..."
            value={textComment}
            name="textComments"
            onChange={e => setTextComment(e.target.value)}
          />
        </form>
      </div>
      <SendComment className="sendComment" onClick={submitСomment} />
    </div>
  );
};
export default AddСomment;
