import axios from 'axios';
import useAuth from '../../../hooks/useAuth';
import './style.scss';

import { ReactComponent as Delete } from '../../../assets/delete.svg';
import { ReactComponent as Back } from '../../../assets/undo.svg';
import { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Link, useParams } from 'react-router-dom';
import { url } from '../../../config';
import AddСomment from './AddСomment';
import ItemComment from './ItemComment';
import Avatar from '../../../component/avatar';

const PagePost = () => {
  const { user } = useAuth();
  const { article } = useAuth();
  const { post } = useAuth();

  const { id } = useParams();

  const [itemComment, setItemComment] = useState([]);
  const [articlePost, setArticlePost] = useState(null);
  const [counter, setCounter] = useState(1);

  const history = useHistory();
  const handleHistory = () => {
    history.push('/');
  };

  const showComment = () => {
    setCounter(prev => prev + 1);
  };

  useEffect(() => {
    if (article?.length > 0) {
      if (article.filter(iteam => iteam._id === id)[0]) {
        setArticlePost(article.filter(iteam => iteam._id === id)[0]);
      } else {
        history.push('/');
      }
    }
  }, [article, id, history]);

  const submitDelete = () => {
    handleHistory();
    post.rm(id);
  };

  useEffect(() => {
    axios
      .get('https://22k.space/api/comment/' + id, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('tokenToken')}`,
        },
      })
      .then(res => {
        console.log(res);
        setItemComment(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  }, [id]);

  return (
    <div className="wrapperPagePost">
      <div className="wrapperPagePostTitle">
        <div className="pagePostTitle">{articlePost?.title}</div>
        <Link to="/" className="closePagePost">
          <Back className="backPageAdmin" />
        </Link>
      </div>

      <div className="containerPhotoDescription">
        <div className="containerDescription">
          <div className="containerAuthor">
            <div className="authorPostPhoto">
              <Avatar user={articlePost?.author} />
            </div>

            <div>
              <div className="authorPostName">
                {articlePost?.author.name + ' ' + articlePost?.author.surName}
              </div>
              <div className="authorPostDate">
                {new Date(Number(articlePost?.createdAt)).toLocaleString()}
              </div>
            </div>
          </div>

          <div className="pagePostDescription">{articlePost?.description}</div>
          {user?._id === articlePost?.author._id && (
            <div onClick={() => submitDelete()}>
              <Delete className="deletePost" />
            </div>
          )}
        </div>

        <div className="containerPhoto">
          <img src={url + '/' + articlePost?.cover} alt="pagePostImg" />
        </div>
      </div>

      <AddСomment setItemComment={setItemComment} id={id} />

      <div className="containerComment">
        {itemComment.slice(0, 5 * counter).map(item => (
          <ItemComment item={item} key={item._id} />
        ))}
      </div>

      {itemComment.length >= 5 * counter && (
        <div className="showMoreComments" onClick={showComment}>
          Show more comments
        </div>
      )}
    </div>
  );
};

export default PagePost;
