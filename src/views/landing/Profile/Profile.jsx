
import { useEffect, useState } from 'react';
import { ReactComponent as EditImg } from '../../../assets/edit.svg';
import { url } from '../../../config';
import useAuth from '../../../hooks/useAuth';
import './style.scss';

const Profile = () => {
  const { user } = useAuth();
  const { avatarChange } = useAuth();
  const { userDataChange } = useAuth();

  const [data, setData] = useState({});
  const [edit, setEdit] = useState(false);

  useEffect(() => {
    setData({ ...user, role: 'Admin' });
  }, [user]);

  const change = (name, value) => {
    setData(prev => {
      return {
        ...prev,
        [name]: value,
      };
    });
  };

  const submitAvatar = fileAvatar => {
    avatarChange(fileAvatar);
  };

  const submit = () => {
    if (edit === false) {
      setEdit(true);
    } else {
      userDataChange(data.name, data.surName, data.phone);
      setEdit(false);
    }
  };

  return (
    <div className="wrapperProfile">
      <input
        type="file"
        name="fileAvatar"
        id="fileAvatar"
        onChange={e => submitAvatar(e.target.files[0])}
        className="inputFileAvatar"
        accept=".jpg, .png, .jpeg"
      />

      {data?.avatar ? (
        <label className="photoProfile" htmlFor="fileAvatar">
          <img src={url + '/' + data.avatar} alt="" />
        </label>
      ) : (
        <label className="photoProfileSign" htmlFor="fileAvatar">
          <div>{data?.email?.[0]}</div>
        </label>
      )}

      <div className="infoProfile">
        <div className="block">
          <label>Name surName:</label>
          {!edit ? (
            <div className="infoName">{`${data.name || ''} ${
              data.surName || ''
            }`}</div>
          ) : (
            <>
              <input
                value={data.name}
                onChange={e => change('name', e.target.value)}
              />
              <input
                value={data.surName}
                onChange={e => change('surName', e.target.value)}
              />
            </>
          )}
        </div>
        <div className="block">
          <label>Email:</label>
          {!edit ? (
            <div className="infoEmail">{data.email}</div>
          ) : (
            <input
              onChange={e => change('email', e.target.value)}
              value={data.email}
              disabled
            />
          )}
        </div>

        <div className="block">
          <label>Number:</label>
          {!edit ? (
            <div className="infoNumber">{data.phone}</div>
          ) : (
            <input
              onChange={e => change('phone', e.target.value)}
              value={data.phone}
            />
          )}
        </div>
        <div className="editButton" onClick={submit}>
          <EditImg />
        </div>
      </div>
    </div>
  );
};

export default Profile;
