import { useState } from 'react';
import axios from 'axios';
import { ReactComponent as LogoImg } from '../../../assets/photos.svg';
import './style.scss';

const SignUp = ({ createNotify, setState }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirm, setConfirm] = useState('');

  const submit = e => {
    e.preventDefault(email, password, confirm);

    if (password.length < 4) {
      createNotify({
        title: 'Failed registration',
        message: 'Need more characters to flog',
        color: '#f44336',
      });
      return;
    }

    if (password !== confirm) {
      createNotify({
        title: 'Failed registration',
        message: 'Do not match Passwords',
        color: '#f44336',
      });
      return;
    }

    axios
      .post('https://22k.space/api/auth/registration', { email, password })
      .then(res => {
        setState('SignIn');
      })
      .catch(err => {
        createNotify({
          title: 'Failed registration',
          message: err.message,
          color: '#f44336',
        });
      });
  };

  return (
    <div className="containerForm">
      <form className="iteamForm" onSubmit={submit}>
        <div className="iteamLogoImg">
          <LogoImg />
        </div>

        <h3>Сreate your Account</h3>

        <div className="inputWrapper">
          <label>Email:</label>
          <input
            value={email}
            onChange={e => setEmail(e.target.value)}
            type="email"
            name="name"
            required
          />
        </div>

        <div className="inputWrapper">
          <label>Password:</label>
          <input
            value={password}
            onChange={e => setPassword(e.target.value)}
            type="password"
            name="name"
            required
          />
        </div>

        <div className="inputWrapper">
          <label>Confirm Password:</label>
          <input
            value={confirm}
            onChange={e => setConfirm(e.target.value)}
            type="password"
            name="name"
            required
          />
        </div>

        <button className="iteamAddSingIn">create an account</button>

        <div className="iteamTab">
          <div
            onClick={() => {
              setState('SignIn');
            }}
          >
            Sign In
          </div>
        </div>

        <div className="coppyright">22karpenko © Your Website 2021.</div>
      </form>
    </div>
  );
};

export default SignUp;
