import { useState } from 'react';

import SignIn from './SignIn/SignIn';
import SignUp from './SignUp/SingUp';
import './style.scss';

const Auth = ({ createNotify }) => {
  const [state, setState] = useState('SignIn');

  return (
    <div className="containerAuth">
      {state === 'SignIn' && (
        <SignIn createNotify={createNotify} setState={setState} />
      )}
      {state === 'SignUp' && (
        <SignUp createNotify={createNotify} setState={setState} />
      )}
    </div>
  );
};

export default Auth;
